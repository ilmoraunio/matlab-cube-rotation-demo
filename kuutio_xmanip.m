function Cube = kuutio_xmanip(Cube, alpha)
	for i = 1:8
		Cube(i).x = [1 0 0; 0 cosd(alpha) -sind(alpha);0 sind(alpha) cosd(alpha);] * Cube(i).x;
	end;
	return;
end