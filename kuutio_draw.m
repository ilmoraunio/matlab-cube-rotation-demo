function kuutio_draw (vector)
	plot3(vector.x, vector.y, vector.z);
		
    xlabel('x');
    ylabel('y');
    zlabel('z');

    xlim([-3 3]);
    ylim([-3 3]);
    zlim([-3 3]);
	return;
end