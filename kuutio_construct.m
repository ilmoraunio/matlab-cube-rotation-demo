function Cube = kuutio_construct()
	Cube(1).x = [1 0 1;]';
	Cube(2).x = [-1 0 1;]';
	Cube(3).x = [-1 0 -1;]';
	Cube(4).x = [1 0 -1;]';
	Cube(5).x = [1 1 1;]';
	Cube(6).x = [-1 1 1;]';
	Cube(7).x = [-1 1 -1;]';
	Cube(8).x = [1 1 -1;]';
	
	return;
end