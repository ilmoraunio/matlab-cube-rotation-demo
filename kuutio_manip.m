function Cube = kuutio_manip(Cube, alpha, axis)
	if axis == 'x'
		for i = 1:8
			Cube(i).x = [1 0 0;0 cosd(alpha) -sind(alpha);0 sind(alpha) cosd(alpha);] * Cube(i).x;
		end;
	elseif axis == 'y'
		for i = 1:8
			Cube(i).x = [cosd(alpha) 0 sind(alpha);0 1 0;-sind(alpha) 0 cosd(alpha);] * Cube(i).x;
		end;
	elseif axis == 'z'
		for i = 1:8
			Cube(i).x = [cosd(alpha) -sind(alpha) 0;sind(alpha) cosd(alpha) 0;0 0 1;] * Cube(i).x;
		end;
	end;
	return;
end