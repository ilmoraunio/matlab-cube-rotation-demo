Will rotate a cube along either x, y or z axis. 

Just run kuutio(rotationDegrees, axis), where rotationDegrees is how much radian degrees to rotate it by and axis is either 'x', 'y' or 'z'.

Tested with Matlab only (run pathdef.m in Matlab before calling the function above).

Ps. comments are in Finnish (sorry about that!)