function cubeSides = kuutio_sides(Cube)
	cubeSides = struct('top', [Cube(5).x Cube(6).x Cube(7).x Cube(8).x Cube(5).x;], 'first', [Cube(1).x Cube(5).x Cube(8).x Cube(4).x Cube(1).x;], 'second', [Cube(1).x Cube(2).x Cube(6).x Cube(5).x Cube(1).x;], 'third', [Cube(2).x Cube(6).x Cube(7).x Cube(3).x Cube(2).x;], 'fourth', [Cube(3).x Cube(7).x Cube(8).x Cube(4).x Cube(3).x;], 'bottom', [Cube(1).x Cube(2).x Cube(3).x Cube(4).x Cube(1).x;]);
	
	return;
end