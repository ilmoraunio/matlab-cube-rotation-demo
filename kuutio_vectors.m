function combinationVector = kuutio_vectors(Sides)
	x = [Sides.top(1,:), Sides.first(1,:), Sides.second(1,:), Sides.third(1,:), Sides.fourth(1,:), Sides.bottom(1,:)];
	y = [Sides.top(2,:), Sides.first(2,:), Sides.second(2,:), Sides.third(2,:), Sides.fourth(2,:), Sides.bottom(2,:)];
	z = [Sides.top(3,:), Sides.first(3,:), Sides.second(3,:), Sides.third(3,:), Sides.fourth(3,:), Sides.bottom(3,:)];
	
	combinationVector = struct('x', x', 'y', y', 'z', z');
	return;
end