% kuutio-funktion parametri annetaan radiaaneina
function kuutio = kuutio(alpha, axis)
	Cube = kuutio_construct();

	% Rakennetaan kuution lopullinen pistetaulukko
	finalCube = kuutio_manip(Cube, -alpha, axis);
	Sides = kuutio_sides(finalCube);
	finalvector = kuutio_vectors(Sides);

	figure;

	set(gca, 'XTick', 0:1:2);
	set(gca, 'YTick', 0:1:2);
	set(gca, 'ZTick', 0:1:2);
    
	view(3);

    % Piirretään animaatio
	for i = 0:abs(alpha)-1
		
		% Tarkistetaan piirtosuunta
		% Vastapäivä
		if alpha > 0
			Cube = kuutio_manip(Cube, -1, axis); % astevakio
		% Myötäpäivä
		else
			Cube = kuutio_manip(Cube, 1, axis); % astevakio
		end;

		Sides = kuutio_sides(Cube);
		vector = kuutio_vectors(Sides);
		kuutio_draw(vector);

		pause(.001);
	end;
    
	% Viimeinen (ja muita täsmällisempi) kuutiopistetaulukko piirretään koordinaatistoon
	kuutio_draw(finalvector);

	kuutio = finalCube;
	return;
end